<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 1/31/2019
 * Time: 10:36 PM
 */

namespace App\Libraries;


use App\Libraries\Interfaces\ILedisDefaultAction;
use Illuminate\Support\Facades\Cache;

class Ledis implements ILedisDefaultAction
{
    const KEYS = "ALL_KEYS_OF_THIS_APP";
    use ListActionTrait;

    public function get(string $key)
    {
        return Cache::get($key);
    }

    public function set(string $key, $value = null)
    {
        Cache::forever($key, $value);
        $this->_afterSet($key);
    }

    public function clear(string $key)
    {
        return Cache::forget($key);
    }

    public function clearAll()
    {
        return Cache::flush();
    }

    public function keys() {
        return Cache::get(static::KEYS);
    }

    // hooks
    private function _afterSet($key) {
        $this->push(static::KEYS, [$key]);
    }
}