<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 1/31/2019
 * Time: 10:42 PM
 */

namespace App\Libraries;


use Illuminate\Support\Facades\Cache;

trait ListActionTrait
{
    public function getLength(string $key) {
        $value = Cache::get($key);
        if (empty($value) || !is_array($value)) {
            return 0;
        }

        return count($value);
    }

    public function push(string $key, array $values) {
        $value = Cache::get($key);

        if (empty($value)) {
            Cache::forever($key, $values);
            $this->_afterSet($key);
        } else {
            // change to array??
            if (!is_array($value)) {
                $value = [$value];
            }

            // push data
            foreach ($values as $val) {
                $value[] = $val;
            }

            //re-set
            Cache::forever($key, $value);
        }
    }

    public function pop(string $key) {
        $value = Cache::get($key);
        if (empty($value) || !is_array($value)) {
            return null;
        }

        $pop_value = array_pop($value);
        //re-set
        Cache::forever($key, $value);

        return $pop_value;
    }

    public function shift(string $key) {
        $value = Cache::get($key);
        if (empty($value) || !is_array($value)) {
            return null;
        }

        $shift_vl = array_shift($value);
        //re-set
        Cache::forever($key, $value);

        return $shift_vl;
    }
}