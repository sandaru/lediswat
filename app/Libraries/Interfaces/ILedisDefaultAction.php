<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 1/31/2019
 * Time: 10:36 PM
 */

namespace App\Libraries\Interfaces;


interface ILedisDefaultAction
{
    public function get(string $key);
    public function set(string $key, $value = null);
    public function clear(string $key);
    public function clearAll();
}