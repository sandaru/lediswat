<?php
/**
 * Created by PhpStorm.
 * User: Seth Phat
 * Date: 1/31/2019
 * Time: 10:49 PM
 */

namespace App\Http\Controllers;


use App\Libraries\Ledis;

class APIController extends Controller
{
    const ACTION_MAPPING = [
        'SET' => true,
        'GET' => true,
        'LLEN' => true,
        'RPUSH' => true,
        'LPOP' => true,
        'RPOP' => true,
        'KEYS' => true,
    ];


    public function action() {
        $action = request()->post('action', null);
        if (empty($action)) {
            return response()->json(['msg' => 'Where is your input?']);
        }

        // get the information
        $splitted = explode(" ", $action);
        $action_type = array_shift($splitted);

        if (!isset(static::ACTION_MAPPING[$action_type]) || static::ACTION_MAPPING[$action_type] == false) {
            return response()->json(['msg' => 'Your command is not supported yet.']);
        }

        // ok run
        $ledis = new Ledis();
        $result = "";

        // finished it here...
        switch ($action_type) {
            case 'GET': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }

                $result = $ledis->get($splitted[0]);
                return response()->json(['data' => $result]);
            }
            case 'SET': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }
                if (!isset($splitted[1]) || empty($splitted[1])) {
                    return response()->json(['msg' => 'Missing value']);
                }

                $ledis->set($splitted[0], $splitted[1]);
                return response()->json(['msg' => 'OK']);
            }
            case 'LLEN': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }

                $result = $ledis->getLength($splitted[0]);
                return response()->json(['data' => $result]);
            }
            case 'LPOP': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }

                $result = $ledis->shift($splitted[0]);
                return response()->json(['data' => $result]);
            }
            case 'RPOP': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }

                $result = $ledis->pop($splitted[0]);
                return response()->json(['data' => $result]);
            }
            case 'RPUSH': {
                if (!isset($splitted[0]) || empty($splitted[0])) {
                    return response()->json(['msg' => 'Missing key']);
                }

                $key = array_shift($splitted);
                $ledis->push($key, $splitted);
                return response()->json(['msg' => 'OK']);
            }
            case 'KEYS': {
                $result = $ledis->keys();
                return response()->json(['data' => $result]);
            }
            default: {
                return response()->json(['msg' => 'Not supported']);
            }
        }
    }

}